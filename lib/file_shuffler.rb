# I/O Exercises
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def file_shuffler
  puts "what file would you like shuffled?"
  input = gets.chomp
  file_name = input.split('.').first
  extension = input.split('.').last
  # shuffled = File.new("#{input}-shuffled.txt")
  lines = File.readlines(input)
  shuffled = lines.shuffle
  File.open("#{file_name}-shuffled.#{extension}", "w") do |f|
    shuffled.each do |line|
      f.puts line
    end
  end
  # lines.shuffle.each do |line|
  #   shuffled.puts line
  # end
end

file_shuffler
